#import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import time
#import requests
from splinter import Browser
import pymongo
from flask import Flask, render_template

# Initialize PyMongo to work with MongoDBs
conn = 'mongodb://localhost:27017'
client = pymongo.MongoClient(conn)

# Define database and collection
db = client.mars
collection = db.items


def get_db_documents():
    mars_news = []
    url = 'https://mars.nasa.gov/news/'
    #browser = Browser()
    executable_path = {'executable_path': '/usr/local/bin/chromedriver'}
    browser = Browser('chrome', **executable_path, headless=False)
    
    browser.visit(url)
    time.sleep(5)
    html = browser.html
    soup = BeautifulSoup(html, 'html.parser')
    
    # data structures
    posting = {}
    titles = []
    content = []   
    
    title_results = soup.find_all("div", {"class": "content_title"})
    teaser_results = soup.find_all("div", {"class": "article_teaser_body"})

    for result in title_results:
        title = result.find('a').text
        titles.append(title)

    for teaser in teaser_results:
        b = teaser.get_text()
        content.append(b)
    # not all 'content_title' tags have 'article_teaser_body' content
    # loop through all content, ignoring 'content_title without data'

    for i in range(1):
        posting = {'title': titles[i], 'text': content[i]}
        mars_news.append(posting)
        #coll.insert_one(posting)
        
    return mars_news
    

def mars_featured_image():
    pic_url = 'https://www.jpl.nasa.gov/spaceimages/?search=&category=Mars'
    #browser = Browser()
    executable_path = {'executable_path': '/usr/local/bin/chromedriver'}
    browser = Browser('chrome', **executable_path, headless=False)
    
    browser.visit(pic_url)
    time.sleep(5)
    html = browser.html
    soup = BeautifulSoup(html, 'html.parser')
    
    
    image = soup.find("a", {"class": "button fancybox"})
    med_size = image.attrs['data-fancybox-href']
    large_size = med_size.replace('mediumsize', 'largesize')
    large_size = large_size.replace('_ip', '_hires')
    large_link = 'https://www.jpl.nasa.gov' + large_size
    
    return large_link
   

def mars_weather():
    twitter_url = 'https://twitter.com/marswxreport?lang=en'
    #browser = Browser()
    executable_path = {'executable_path': '/usr/local/bin/chromedriver'}
    browser = Browser('chrome', **executable_path, headless=False)
    
    browser.visit(twitter_url)
    time.sleep(5)
    html = browser.html
    soup = BeautifulSoup(html, 'html.parser')
    
    browser.visit(twitter_url)

    html = browser.html
    soup = BeautifulSoup(html, 'html.parser')
    
    weather_container = soup.find("p", {"class": "TweetTextSize TweetTextSize--normal js-tweet-text tweet-text"})
#    print(weather_container)
    forecast = weather_container.text
#    print(forecast)


    
    mars_weather = forecast.replace(weather_container.find('a').text, '')
    
    return mars_weather


def mars_facts():
    mars_facts_url = 'https://space-facts.com/mars/'
    import pandas as pd
    
    #browser = Browser()
    executable_path = {'executable_path': '/usr/local/bin/chromedriver'}
    browser = Browser('chrome', **executable_path, headless=False)
    
    browser.visit(mars_facts_url)
    time.sleep(5)
    html = browser.html
    soup = BeautifulSoup(html, 'html.parser')
    
    mars_facts_table = soup.find("table", {"class": "tablepress tablepress-id-p-mars"})
    df_mars_facts = pd.read_html(str(mars_facts_table))
    
    return df_mars_facts


def mars_hemispheres_fetch_links():
    astrogeology_url = 'https://astrogeology.usgs.gov/search/results?q=hemisphere+enhanced&k1=target&v1=Mars'
    astro_rel_links = []
    

    executable_path = {'executable_path': '/usr/local/bin/chromedriver'}
    browser = Browser('chrome', **executable_path, headless=False)
    
    browser.visit(astrogeology_url)
    time.sleep(5)
    html = browser.html
    soup = BeautifulSoup(html, 'html.parser')
    
    astrogeology_items = soup.find_all("div", {"class": "description"})
    astro_rel_links = []
    for astro in astrogeology_items:
        rel_link = astro.find('a')
        astro_rel_links.append(rel_link.attrs['href'])
    
    return(astro_rel_links)
    

def mars_hemispheres(url, hemi_list):
    hemi_dict = {}
    base_asrogeology_url = 'https://astrogeology.usgs.gov'
    astro_url = base_asrogeology_url + url
    executable_path = {'executable_path': '/usr/local/bin/chromedriver'}
    browser = Browser('chrome', **executable_path, headless=False)
    browser.visit(astro_url)
  

    time.sleep(8)
    
    html = browser.html
    soup = BeautifulSoup(html, 'html.parser')
    
    link_section = soup.find_all("section", {"class": "block metadata"})
    for section in link_section:
        alink = section.find('a')
        hemi_dict['title'] = section.find('h2').text
        hemi_dict['img_url'] = alink.attrs['href']
        hemi_list.append(hemi_dict)
    
    return hemi_list




def scrape():
    mars_scraped_data = {}
    
    mars_news_list = []
    hemi_image_urls = []
    
    featured_image_url = mars_featured_image()
    mars_weather_string = mars_weather()
    mars_news_list = get_db_documents()
    mars_facts_df = mars_facts()
    
    astrogeology_relative_links = mars_hemispheres_fetch_links()
    for astro in astrogeology_relative_links:
        hemi_image_urls = mars_hemispheres(astro, hemi_image_urls)
    
    
#    mars_scraped_data['news_title': mars_news_list[0]['title'],
#                      'news_p':mars_news_list[0]['text'],
#                      'featured_image_url': featured_image_url,
#                      'mars_weather': mars_weather,
#                      'mars_df': mars_facts_df,
#                      'hemisphere_image_urls': hemi_image_urls]
    
    mars_scraped_data['news_title'] = mars_news_list[0]['title']
    mars_scraped_data['news_p'] = mars_news_list[0]['text']
    mars_scraped_data['featured_image_url'] = featured_image_url
    mars_scraped_data['mars_weather'] = mars_weather_string
    mars_scraped_data['mars_df'] = str(mars_facts_df)
    mars_scraped_data['hemisphere_image_urls'] = hemi_image_urls
    
    #print(mars_scraped_data)
    #collection.insert_one(mars_scraped_data)
    return (mars_scraped_data)

#all_data = scrape()   
#print(all_data)
#collection.insert_one(all_data)


# # Create an instance of our Flask app.
# app = Flask(__name__)

# # Drops collection if available to remove duplicates
# db.items.drop()

# db.items.insert_one(all_data)

# # Set route
# @app.route('/')
# def index():
#     # Store the entire team collection in a list
#     mars = list(db.items.find())
#     print(mars)

#     # Return the template with the teams list passed in
#     return render_template('index.html', mars=mars)


# if __name__ == "__main__":
#     app.run(debug=True)
