from flask import Flask, render_template, redirect
from flask_pymongo import PyMongo
import scrape_mars
import requests

app = Flask(__name__)

# Use flask_pymongo to set up mongo connection
app.config["MONGO_URI"] = "mongodb://localhost:27017/mars"
mongo = PyMongo(app)

# Or set inline
# mongo = PyMongo(app, uri="mongodb://localhost:27017/craigslist_app")


@app.route("/")
@app.route('/index')
def index():
    #items = mongo.db.mars.find_one()
    mars = mongo.db
    use_col = mars['items']
    items = use_col.find_one()
    #response = requests.get(items.hemisphere_image_urls.img_url)
    #print(items)
    return render_template("/index.html", items=items)


    
    #'{}/files/newdata.csv'.format(API_URL), headers=headers


@app.route("/scrape")
def scraper():
    mars = mongo.db
    use_col = mars['items']
    mars_data = scrape_mars.scrape()
    print(type(mars_data))
    #scrape_mars.scrape()
    #print(mars_data)
    use_col.insert_one(mars_data)
    #mars.items.update({}, mars_data, upsert=True)
    #mars.insert_one(mars_data)
    return redirect("/index", code=302)


if __name__ == "__main__":
    app.run(debug=True)